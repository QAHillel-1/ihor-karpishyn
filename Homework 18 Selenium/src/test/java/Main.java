import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;



import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;


public class Main {
    WebDriver driver = new ChromeDriver();
    UserForTest user = new UserForTest();
    final Wait<WebDriver> wait = new WebDriverWait(driver, 5).withMessage("Element was not found");



    @BeforeClass
    public static void beforeClass(){
        final String path = String.format("%s/bin/chromedriver.exe", System.getProperty("user.dir"));
        System.setProperty("webdriver.chrome.driver",path);


    }


    @Test
    public void firstTest()  {
        driver.get("https://user-data.hillel.it/html/registration.html");
        driver.manage().window().maximize();
        driver.findElement(By.className("registration")).click();
        driver.findElement(By.id("first_name")).sendKeys(user.getFirstName());
        driver.findElement(By.id("last_name")).sendKeys(user.getLastName());
        driver.findElement(By.id("field_work_phone")).sendKeys(user.getWorkPhone());
        driver.findElement(By.id("field_phone")).sendKeys(user.getPhone());
        driver.findElement(By.id("field_email")).sendKeys(user.getEmail());
        driver.findElement(By.id("field_password")).sendKeys(user.getPassword());
        driver.findElement(By.id(user.getGender())).click();
        Select dropdown = new Select(driver.findElement(By.id("position")));
        dropdown.selectByVisibleText(user.getPosition());
        driver.findElement(By.id("button_account")).click();
        driver.navigate().refresh();

        wait.until(visibilityOfElementLocated(By.className("login_form")));
        driver.findElement(By.id("email")).sendKeys(user.getEmail());
        driver.findElement(By.id("password")).sendKeys(user.getPassword());
        driver.findElement(By.className("login_button")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("employees")));
        driver.findElement(By.linkText("Employees")).click();

        driver.findElement(By.id("mobile_phone")).sendKeys(user.getPhone());
        driver.findElement(By.id("search")).click();


        System.out.println(user.getGender());
        System.out.println(user.getPosition());

        wait.until(ExpectedConditions.numberOfElementsToBe(By.xpath("//table/tr"),1));
        String actualName = driver.findElement(By.xpath("//table/tr/td[1]")).getText();
        String actualLastName = driver.findElement(By.xpath("//table/tr/td[2]")).getText();
        String actualEmail = driver.findElement(By.xpath("//table/tr/td[3]")).getText();
        String actualMobPhone = driver.findElement(By.xpath("//table/tr/td[4]")).getText();
        String actualPhone = driver.findElement(By.xpath("//table/tr/td[5]")).getText();
        String actualGender = driver.findElement(By.xpath("//table/tr/td[6]")).getText();
        String actualPosition = driver.findElement(By.xpath("//table/tr/td[7]")).getText();

        Assert.assertEquals("Wrong name",user.getFirstName(),actualName);
        Assert.assertEquals("Wrong last name",user.getLastName(),actualLastName);
        Assert.assertEquals("Wrong Email",user.getEmail(),actualEmail);
        Assert.assertEquals("Wrong Mobile Phone number",user.getPhone(),actualMobPhone);
        Assert.assertEquals("Wrong phone number",user.getWorkPhone(),actualPhone);
        Assert.assertEquals("Wrong gender",user.getGender(),actualGender);
        Assert.assertEquals("Wrong position",user.getPosition(),actualPosition);
        

    }


    @After
    public void after() throws InterruptedException {
        //Почистим за собой. 5 секунд визуально убедиться, удалить
        Thread.sleep(5000);
        driver.findElement(By.xpath("//table/tr/td[9]")).click();
        // 5 секунд визуально увидеть что удалено
        Thread.sleep(5000);
        driver.quit();
    }

    @AfterClass
    public static void afterClass(){
        System.out.println("It's ALIVE!!!!");
    }
}
