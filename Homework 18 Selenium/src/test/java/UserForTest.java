public class UserForTest {
    private String firstName="Ihor";
    private String lastName="Selenium";
    private String workPhone="12345";
    private String phone;
    private String email;
    private String password;
    private String gender;
    private String position;

    UserForTest(){
        randomEmail();
        randomPassword();
        randomPosition();
        randomGender();
        randomMobilePhone();
    }


    private void randomEmail() {
        String email = "";
        String possible = "1234567890abcdefghijklmnopqrstuvwxyz";
        for (int i = 0; i < 3; i++) {
            email += possible.charAt((int) Math.floor(Math.random() * possible.length()));
        }
        this.email="ihor_" + email + '@' + "gmail.com";
    }

    private void randomPassword() {
        String pass = "";
        String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lower  = "abcdefghijklmnopqrstuvwxyz";
        String symb = "0123456789";

        for (int i = 1; i <= 4; i++) {
            char char1=upper.charAt((int) Math.floor(Math.random() * upper.length()));
            char char2 = lower.charAt((int) Math.floor(Math.random() * lower.length()));
            char char3 = symb.charAt((int) Math.floor(Math.random() * symb.length()));
            pass += char1;
            pass += char2;
            pass += char3;
        }
        this.password=pass;
    }

    private void randomPosition() {
        String[] Positions = {"qa", "developer", "manager"};
        String position = Positions[(int) Math.floor(Math.random() * Positions.length)];
        this.position = position;
    }

    private void randomGender(){
        String [] genders = {"male", "female"};
        String gender = genders[(int) Math.floor(Math.random()*genders.length)];
        this.gender=gender;
    }
    private void randomMobilePhone() {
        String mobPhone = "";
        String possible = "123456789";
        for (int i = 0; i < 7; i++) {
            mobPhone += possible.charAt((int)Math.floor(Math.random() * possible.length()));
        }
        this.phone="38093"+mobPhone;
    }

    public String getEmail(){
        return email;
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public String getPhone() {
        return phone;
    }

    public String getGender() {
        return gender;
    }

    public String getPassword() {
        return password;
    }

    public String getPosition() {
        return position;
    }

    public void setFirstName(String name){
        this.firstName=name;
    }
}
