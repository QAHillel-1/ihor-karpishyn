package com.company;
import java.util.*;

/*Необходимо вывести на экран числа от 5 до 1. На экране должно быть:
5 4 3 2 1
*/

public class Main {

    public static void main(String[] args) {
        for (int i=5; i>0; i--) {
            System.out.print(i + " ");
        }
    }
}
