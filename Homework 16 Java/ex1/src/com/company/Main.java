package com.company;
import java.util.*;

/* Необходимо написать программу, где бы пользователю предлагалось ввести число на выбор:
      1, 2 или 3, а программа должна сказать, какое число ввёл пользователь: 1, 2, или 3.
      Написать двумя способами. If и switch.
      */
      /*System.out.println("Введите число 1, 2 или 3: ");
      Scanner inputFigure = new Scanner(System.in);
      int i = inputFigure.nextInt();
      System.out.println(i);*/

public class Main {

    public static void main(String[] args) {
        System.out.println("Введите число 1, 2 или 3: ");
        Scanner inputFigure = new Scanner(System.in);
        if (inputFigure.hasNextInt()) {
            int input = inputFigure.nextInt();
            // Через for
            if (input == 1 || input == 2 || input == 3) {
                if (input == 1) {
                    System.out.println("(через for) Вы ввели 1");
                } else if (input == 2) {
                    System.out.println("(через for) Вы ввели 2");
                } else {
                    System.out.println("(через for) Вы ввели 3");
                }
            } else {
                System.out.println("(через for) Только 1, 2 или 3");
            }
            // теперь через switch
            switch (input){
                case 1:
                    System.out.println("(Теперь через switch) Вы ввели 1");
                    break;
                case 2:
                    System.out.println("(Теперь через switch) Вы ввели 2");
                    break;
                case 3:
                    System.out.println("(Теперь через switch) Вы ввели 3");
                    break;
                default:
                    System.out.println("(Теперь через switch) Только 1, 2 или 3");
                    break;
            }
        } else {
            System.out.println("Вы ввели не число");
        }



    }
}
