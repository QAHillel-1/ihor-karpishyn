package com.company;

// Найти среднее значение суммы чисел от 1 до 100

public class Main {

    public static void main(String[] args) {
        int sum=0;
        for (int i = 1; i<101; i++){
            sum+=i;
        }
        System.out.println("Среднее значение суммы чисел от 1 до 100 = "+(double)sum/100);
    }
}
