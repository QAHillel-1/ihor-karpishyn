/*
Даны переменные a и b. Проверьте, что a делится без остатка на b.
Если это так - выведите 'Делится'и результат деления,
иначе выведите 'Делится с остатком' и остаток от деления. Решение реализовать в виде функции
*/

'use strict';

function checkForValidData (a,b) {
    if (isNaN (parseInt(a)) || isNaN(parseInt(b))){
        window.alert("Нужно вводить только числа!!!!!");
    } else if (b == 0) {
        window.alert("На ноль делить нельзя!!!!");
      } else { return true; }
}

function divide (a,b) {
    if (a % b == 0) {
        window.alert("Делится " + a / b);
    } else {
        window.alert("Делится с остатком " + a % b);
    }
}



let a = prompt("Введите делимое ", "");
let b = prompt("Введите делитель ", "");
if (checkForValidData(a,b) == true){
    divide(a,b);
}


