package com.company;

public class User {
    private String firstName;
    private String lastName;
    private String workPhone;
    private String mobilePhone;
    private String email;
    private String password;

    User (String firstName, String lastName, String workPhone, String mobilePhone, String email, String password){
        this.firstName=firstName;
        this.lastName=lastName;
        this.workPhone=workPhone;
        this.mobilePhone=mobilePhone;
        if (checkEmail(email)){
            this.email=email;
        }
        if (checkPassword(password) == true) {
            this.password = password;
        }

    }

    User (String email, String password){
        if (checkEmail(email)){
            this.email=email;
        }
        if (checkPassword(password) == true) {
            this.password = password;
        }
    }


    private Boolean checkEmail (String email) {
        boolean isFind = false;
        for (int i=0; i< email.length(); i++){
            if (email.charAt(i) == '@') {
                isFind=true;
                break;
            }
        }
        if (isFind) {
            System.out.println("Congratulations!!!! Your email " + email + " is set");
        } else {
            System.out.println("The email " + email + " is not valid and will not be set");
        }
        return isFind;
    }

    private Boolean checkPassword (String password) {
        if (password.length()>7 && password.length()<17){
            System.out.println("Congratulations!!!! Your password " + password +" is set");
            return true;
        } else {
            System.out.println("The password " + password + " is not valid and will not be set");
            return false;
        }

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
