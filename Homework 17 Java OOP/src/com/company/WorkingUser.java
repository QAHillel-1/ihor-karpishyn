package com.company;

public class WorkingUser  extends User{
    private int salary;
    private int experience;

    WorkingUser(String firstName, String lastName, String workPhone, String mobilePhone, String email, String password) {
        super(firstName, lastName, workPhone, mobilePhone, email, password);
    }

    WorkingUser(String email, String password) {
        super(email, password);
    }

    WorkingUser(String firstName, String lastName, String workPhone, String mobilePhone, String email, String password, int experience, int salary){
        super(firstName, lastName, workPhone, mobilePhone, email, password);
        this.experience=experience;
        this.salary=salary;
    }


    public void setSalary (int salary){
        this.salary=salary;
    }
    public void setExperience (int exp){
        this.experience=exp;
    }

    public int getSalary(){
        return salary;
    }
    public int getExperience(){ return experience;}

    public int salaryIncrease () {
        if (experience<2) {
            salary+=salary*0.05;
        } else if (experience>=2 && experience <=5){
            salary+=salary*0.1;
        } else {
            salary+=salary*0.15;
        }
        this.salary=salary;
        return salary;

    }


}
